//
//  ViewController.m
//  Calculator
//
//  Created by 姚德义 on 16/11/2.
//  Copyright © 2016年 ustc. All rights reserved.
//
// 76%  +/-  10099  -切換到x可連續運算  切換加減乘除時砍掉上一個符號deleteString"-+"
#import "ViewController.h"
#import "advancedCalculator.h"

@interface ViewController ()

@property (strong, nonatomic) advancedCalculator *calculate;
@property (weak, nonatomic) IBOutlet UITextField *inputResult;

@property int flag;
@end

@implementation ViewController
-(advancedCalculator *)calculator{
    if(!_calculate){
        _calculate=[[advancedCalculator alloc]init];
    }
    return _calculate;
}

/** 基本输入数字和操作符在同一个事件进行处理*/
- (IBAction)touchViewExpression:(UIButton *)sender {

    if([self.inputResult.text length]!=0&&_flag==1){
        NSString *ch=[[sender titleLabel] text];
        if([ch isEqualToString:@"+/-"]||[ch isEqualToString:@"%"]||
           [ch isEqualToString:@"×"]
           ||[ch isEqualToString:@"÷"]
           ||[ch isEqualToString:@"+"]
           ||[ch isEqualToString:@"-"]){
//            self.inputResult.text=@"暫無連續計算";
//            self.calculator.input=nil;
            _flag=1;
//            return;
        }else{
            self.inputResult.text=nil;
//            self.calculator.input=nil;
            _flag=0;
        }
    }

    //这种处理的原因是对x ÷进行实际运算的替换* /，但显示仍然是x +
    if([[[sender titleLabel] text] isEqualToString:@"×"]){
        [self.calculator.input appendString:@"*"];
        
    }else if([[[sender titleLabel] text] isEqualToString:@"÷"]){
        [self.calculator.input appendString:@"/"];
    }else if([[[sender titleLabel] text] isEqualToString:@"%"]){
//        [self.calculator.input appendString:@"-"];
//              NSInteger inputIntValue = [self.inputResult.text intValue]*2;
//              self.inputResult.text=[NSString stringWithFormat:@"%li", (long)inputIntValue];
//              NSLog(@"%@",self.inputResult.text);
//              NSLog(@"inputIntValue  inputIntValue");
//              [self.calculator.input appendString:self.inputResult.text];

        
        [self.calculator.input appendString:@"/"];
        [self.calculator.input appendString:@"100"];
    }
    else if([[[sender titleLabel] text] isEqualToString:@"+/-"]){
        [self.calculator.input appendString:@"-"];
        NSInteger inputIntValue = [self.inputResult.text intValue]*2;
        self.inputResult.text=[NSString stringWithFormat:@"%li", (long)inputIntValue];
        NSLog(@"%@",self.inputResult.text);
        NSLog(@"inputIntValue  inputIntValue");
        [self.calculator.input appendString:self.inputResult.text];
        
    }
    else if([[[sender titleLabel] text] isEqualToString:@"+"]){
        [self.calculator.input appendString:@"+"];
    }else if([[[sender titleLabel] text] isEqualToString:@"-"]){
        [self.calculator.input appendString:@"-"];
    }
    else{
        self.inputResult.text=nil;

        NSLog(@"%@",  self.inputResult.text);
        NSLog(@"calculatorcalculatorcalculator");

        [self.calculator.input appendString:[[sender titleLabel] text]];
        
    }
    
    if([[[sender titleLabel] text] isEqualToString:@"%"] || [[[sender titleLabel] text] isEqualToString:@"+/-"]) {
        NSMutableString *calculateResult=[NSMutableString stringWithString:self.calculator.input];
        [calculateResult appendString:[[sender titleLabel] text]];
        self.inputResult.text=[self.calculator ExpressionCalculate:calculateResult];
        NSMutableString *tempStr=[NSMutableString stringWithString:self.inputResult.text];;
        self.calculator.screen = tempStr;
    }else{
//                         self.inputResult.text=nil;
        NSLog(@"%@",self.inputResult.text);
        NSLog(@"calculatorcalculatorcalculator");
        
        NSMutableString *calculateResult=[NSMutableString stringWithString:self.calculator.input];
        self.inputResult.text=[self.calculator ExpressionCalculate:calculateResult];
        NSMutableString *tempStr=[NSMutableString stringWithString:self.inputResult.text];;
        self.calculator.screen = tempStr;
    }

//    NSMutableString *originalString=[NSMutableString stringWithString:self.inputResult.text];
//    [originalString appendString:[[sender titleLabel] text]];
//    NSLog(@"%@", originalString);
//
//    self.inputResult.text=originalString;
//    self.calculator.screen=originalString;

}

/** 清空输出框数据以及存储的数据*/
- (IBAction)clearInputExpression:(UIButton *)sender {
    [self.calculator clearAll];
    [self.calculator.input appendString:@"0"];
     self.inputResult.text=@"0";
//    self.inputResult.text=nil;
    _flag=0;
}

/** 计算结果*/
- (IBAction)calculateExpression:(UIButton *)sender {
    //标志为1，表明已经按了=键，计算了结果，下次再输入其他表达式会先清空数据

    _flag=1;
    if([self.inputResult.text length]==0){
        [self.calculator.input appendString:@"0"];
        self.inputResult.text=@"0";
        return;
    }

    NSMutableString *calculateResult=[NSMutableString stringWithString:self.calculator.input];
    [calculateResult appendString:[[sender titleLabel] text]];
    self.inputResult.text=[self.calculator ExpressionCalculate:calculateResult];
    NSMutableString *tempStr=[NSMutableString stringWithString:self.inputResult.text];;
    self.calculator.screen = tempStr;
}

/** 显示下一个前景传递来的数据 */
-(void)viewWillAppear:(BOOL)animated{
    self.inputResult.text=self.calculator.screen;
    self.inputResult.text=@"0";
    [self.calculator.input appendString:@"0"];
    NSLog(@"%@", self.calculator.input);
    NSLog(@"%@", self.inputResult.text);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.calculate=[[advancedCalculator alloc]init];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
